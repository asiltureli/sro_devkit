// Credits: @florian0 https://florian0.wordpress.com/2017/12/14/silkroad-online-reconstructing-ui-access-for-the-sake-of-colors/
#pragma once

class IFSystemMessage
{
public:
    void write(int a1, int color, wchar_t* msg, int a2, int a3);

};